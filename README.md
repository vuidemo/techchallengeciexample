# README #

This is the techchallenge skill.

Video: https://www.youtube.com/watch?v=9dePi9-T1lE

Links from todays talk:

Bitbucket:
https://bitbucket.org/vuidemo/techchallenge

AWS:
https://console.aws.amazon.com/console/home?region=us-east-1#

Developer Amazon 
https://developer.amazon.com

APEX
http://apex.run/

Serverless
https://serverless.com/

CircleCI
https://circleci.com/